package com.assessment.springdatabase.controller;

import com.assessment.springdatabase.model.Item;
import com.assessment.springdatabase.repository.ItemRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;



@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class Items {
    private ItemRepository repo;
    public Items(ItemRepository repo){this.repo =repo;}
    @GetMapping("/items")
    public List<Item> itemList(){

        List<Item> list = (List<Item>)repo.findAll();
        System.out.println(list.size());
        return list;
    }

    @PostMapping("/create")
    public Item createItem(@RequestBody Item item){
//        item.setPicture("../../assets/"+item.getPicture());
        repo.save(item);
        return item;
    }
}
